package com.dabm.servcat.api.resource;

import java.util.List;

import com.dabm.servcat.api.model.GeneralSegA;
import com.dabm.servcat.api.repository.GeneralSegARepository;
import com.dabm.servcat.api.repository.GeneralSegARepositoryJdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dabm.servcat.api.model.Categoria;
import com.dabm.servcat.api.repository.CategoriaRepository;

@RestController
@RequestMapping("/categorias")
public class CategoriaResource {

	@Autowired
	private CategoriaRepository categoriaRepository;

	@Autowired
    private GeneralSegARepository generalSegARepository;
	
	@Autowired
	private GeneralSegARepositoryJdbc generalSegARepositoryJdbc;

	@GetMapping
	public List<Categoria> listar() {
		return categoriaRepository.findAll();
	}

    @GetMapping("/sega")
    public Page<GeneralSegA> segAPage(){
    	
    	//https://www.baeldung.com/spring-data-jpa-pagination-sorting
    	//https://github.com/eugenp/tutorials/blob/master/persistence-modules/spring-data-jpa-enterprise-2/src/test/java/com/baeldung/multipledb/ProductRepositoryIntegrationTest.java
    	
    	Pageable firstPageWithTwoElements = PageRequest.of(0, 2);    	
    	Page<GeneralSegA> allProducts = generalSegARepository.findAll(firstPageWithTwoElements);
    	
        return allProducts;
    }
    
    @GetMapping("/seg-a")
    public Page<GeneralSegA> segAList(){
    	
    	//https://www.baeldung.com/spring-data-jpa-pagination-sorting
    	//https://github.com/eugenp/tutorials/blob/master/persistence-modules/spring-data-jpa-enterprise-2/src/test/java/com/baeldung/multipledb/ProductRepositoryIntegrationTest.java
    	
    	//https://www.bezkoder.com/spring-boot-jpa-crud-rest-api/
    	//https://www.bezkoder.com/spring-data-sort-multiple-columns/
    	//https://github.com/bezkoder/spring-boot-jpa-paging-sorting
    	//https://www.bezkoder.com/spring-boot-pagination-filter-jpa-pageable/
    	
    	//https://fabiano-goes.medium.com/api-rest-com-pagina%C3%A7%C3%A3o-usando-spring-data-e-query-9eddb29c9223
    	
    	
    	//https://www.bezkoder.com/spring-boot-jdbctemplate-example-mysql/
    	
    	//https://www.bezkoder.com/category/spring/page/2/
    	//https://www.bezkoder.com/spring-boot-hibernate-oracle/
    	
    	
    	Pageable firstPageWithTwoElements = PageRequest.of(0, 3);    	
    	Page<GeneralSegA> allProducts = generalSegARepository.findByNiinContaining("0001262", firstPageWithTwoElements);
    	
        return allProducts;
    }

}
