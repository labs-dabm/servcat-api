package com.dabm.servcat.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dabm.servcat.api.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
