package com.dabm.servcat.api.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.dabm.servcat.api.model.GeneralSegA;

@Repository
public class GeneralSegARepositoryJdbc {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Value("${owner.name}")
	String owner;

	public List<GeneralSegA> getGeneralSegA() {

		String sql = getSqlAllGeneral();
		List<GeneralSegA> paises;
		paises = getAll(sql);
		return paises;
	}

	private List<GeneralSegA> getAll(String sql) {
		return jdbcTemplate.query(sql, (rs, rowNum) -> {
			GeneralSegA segA = new GeneralSegA();

			segA.setCodigo(rs.getLong("COD_GEN"));
			segA.setFsc(rs.getString("FSC"));
			// TODO: continuar

			return segA;
		});
	}

	private String getSqlAllGeneral() {
		return "SELECT /*+ FIRST_ROWS */ * FROM FEDLOGDB.GENERAL";
	}

}
