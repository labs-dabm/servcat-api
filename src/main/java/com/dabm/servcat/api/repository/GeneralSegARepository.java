package com.dabm.servcat.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.dabm.servcat.api.model.GeneralSegA;

public interface GeneralSegARepository
    extends JpaRepository<GeneralSegA, Long> {
	//extends PagingAndSortingRepository<GeneralSegA, Long> {

	@Override
	Page<GeneralSegA> findAll(Pageable pageable);
	
	//https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
	Page<GeneralSegA> findByNiinContaining(String niin, Pageable pageable);
}
