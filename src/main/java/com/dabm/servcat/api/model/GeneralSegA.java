package com.dabm.servcat.api.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "GENERAL")
public class GeneralSegA {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_CATEGORIA")
	//@SequenceGenerator(name = "SQ_CATEGORIA", sequenceName = "SQ_CATEGORIA", allocationSize = 1)
	@Column(name = "cod_gen")
	private Long codigo;

	private String fsc;
	private String niin;
	private String nsn;
	private String item_name;
	private String inc;
	private String tiic;
	private String rpdmrc;
	private String fmsn;
	private String mgmt_pmi;
	private String mgmt_adp;
	private String mgmt_dml;
	private String mgmt_esdc;
	private String mgmt_cc;
	private String mgmt_hmic;
	private String origem;

	@Override
	public int hashCode() {
		return Objects.hash(codigo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeneralSegA other = (GeneralSegA) obj;
		return Objects.equals(codigo, other.codigo);
	}
}
