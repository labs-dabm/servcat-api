package com.dabm.servcat.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiServcatApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiServcatApplication.class, args);
    }

}
