/* Single line comment */
create TABLE SINGRA.CATEGORIA (
  CODIGO VARCHAR(25) NOT NULL,
  NOME VARCHAR(100) NOT NULL,
  PRIMARY KEY(CODIGO)
);

-- DROP SEQUENCE SINGRA.SQ_IMPORTACAO_SISCAT;
create sequence SINGRA.SQ_CATEGORIA
  start with 1
  maxvalue 999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

INSERT INTO SINGRA.CATEGORIA (CODIGO, NOME) values (SINGRA.SQ_CATEGORIA.nextval, 'Lazer');
INSERT INTO SINGRA.CATEGORIA (CODIGO, NOME) values (SINGRA.SQ_CATEGORIA.nextval, 'Alimentação');
INSERT INTO SINGRA.CATEGORIA (CODIGO, NOME) values (SINGRA.SQ_CATEGORIA.nextval, 'Supermercado');
INSERT INTO SINGRA.CATEGORIA (CODIGO, NOME) values (SINGRA.SQ_CATEGORIA.nextval, 'Farmácia');
INSERT INTO SINGRA.CATEGORIA (CODIGO, NOME) values (SINGRA.SQ_CATEGORIA.nextval, 'Outros');

-- https://flywaydb.org/documentation/database/oracle
-- https://flywaydb.org/documentation/concepts/migrations.html