# Projeto API SERVCAT

### Requisitos:
* Ter uma conta válida no site do [GitLab](http://www.gitlab.com)
* Se possível ter cadastrado as chaves SSH da máquina no seu profile do site.
* Clonando o projeto no repositório do [https://gitlab.com/mar-dabm/dep-60/siscatbr.git](https://gitlab.com/mar-dabm/dep-60/siscatbr.git)

```bash	
# Clonando o projeto do site
$ git clone git@gitlab.com:mar-dabm/dep-60/siscatbr.git

# ou se desejar baixar para outra pasta
$ git clone git@gitlab.com:mar-dabm/dep-60/siscatbr.git webservice-siscatbr

# se não possuir a chave SSH na conta tem baixar via HTTPS
# será pedido usuario/senha da sua conta do http://www.gitlab.com
$ git clone https://gitlab.com/mar-dabm/dep-60/siscatbr.git
```

#### Configurar o Git:

```bash	
# configurar nome e email no nível global do sistema para identificar no commit
$ git config --global user.name "Seu Nome"
$ git config --global user.email "email@provedor.com"

# Para conferir execute
$ git config user.name
$ git config user.email

# Para listar todas as configurações
$ git config --list
```

#### Criando um chave SSH Para criar uma chave privada/publica no git:
Para cadastrar uma chave privado no seu computador proceda conforme comandos abaixo. Após será necessário copiar a chave pública e disponibilizar em seu profile na conta criada no [Git Lab](https://gitlab.com/profile/keys).

```bash
$ ssh-keygen -t rsa -C "seuemails@provedor.com"
# Para listar a chave pública
$ cat ~/.ssh/id_rsa.pub 
# ou se preferir pelo editor de texto padrão ou cacadastrado
$ subl ~/.ssh/id_rsa.pub
```

#### Iniciando Push an existing Git no projeto:

```bash	
# verficar os branches trazidos do repositório com os commits
git remote rename origin old-origin
git remote add origin git@gitlab.com:mar-dabm/dep-60/siscatbr.git
git push -u origin --all
git push -u origin --tags
```
